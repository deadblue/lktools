__author__ = 'deadblue'

import sys
import logging
logging.basicConfig(
    stream=sys.stdout, level=logging.INFO, datefmt='%H:%M:%S',
    format='%(asctime)s %(levelname)s- %(message)s'
)

_logger = logging.getLogger(__name__)

import string

def _flip(seed, option):
    """
    Flip the seed follow the option.
    Assume seed is "ABCD" and option is "1423", then the result is "ADBC".
    :param seed:
    :param option:
    :return:
    """
    buf = []
    for index in option:
        index = int(index) - 1
        buf.append(seed[index])
    return ''.join(buf)

def _grow(seed, steps, level=0, path=''):
    result = []
    for choice, option in enumerate(steps[level], 0):
        node = _flip(seed, option)
        curr_path = '%s%s' % (path, string.ascii_uppercase[choice])
        if level == len(steps) - 1:
            # Reach the end
            result.append({
                'value': node,
                'path': curr_path,
            })
        else:
            # Going deep
            result.extend( _grow(node, steps, level + 1, curr_path) )
    return result

def _get_leaves(seed, steps):
    return _grow(seed, steps)

def _solve(left, right, steps):
    # try from left to right
    _logger.info('Try from left to right ...')
    leaves = _get_leaves(left, steps)
    for leaf in leaves:
        if leaf['value'] == right:
            return leaf['path']
    # try from right to left
    _logger.info('Try from right to left ...')
    steps.reverse()
    leaves = _get_leaves(right, steps)
    for leaf in leaves:
        if leaf['value'] == left:
            return reversed(leaf['path'])
    # reach here means we can not find a path
    _logger.info('Can not find a path!')
    return None

def _main():
    left, right = 'RBYG', 'GBRY'
    steps = [
        ['2413', '1324', '3241'],
        ['3241'],
        ['3124', '2431', '2413']
    ]
    path = _solve(left, right, steps)
    _logger.info('Result: %s', ' '.join(path))

if __name__ == '__main__':
    _main()
# -*- coding: utf-8 -*-

__author__ = 'deadblue'

import logging
import math

from PIL import Image

from lk.maze._types import _Location, Position, Room, Maze
from lk.maze._direction import NORTH, SOUTH, WEST, EAST

_logger = logging.getLogger(__name__)


def parse(img):
    """
    :param img:
    :type img: Image.Image
    :return:
    :rtype Maze
    """
    # convert image to grey mode
    if img.mode != 'L':
        img = img.convert('L')

    # locate maze from image
    l = _locate(img)

    # crop image
    maze_img = img.crop((
        l.left, l.upper, l.right, l.lower
    ))

    # meature wall and room size
    wall_size, room_size = _meature(maze_img)

    # scan rooms
    return _scan_rooms(maze_img, wall_size, room_size).\
        with_origin(l.left, l.upper)


def _locate(img):
    """
    Locate maze from image

    :param img:
    :type img: Image.Image
    :return:
    :rtype: _Location
    """

    origin_x, origin_y = 0, 0
    maze_width, maze_height = 0, 0

    img_w, img_h = img.size
    # find left side
    for x in range(img_w):
        white, black = 0, 0
        top, bottom = -1, -1
        for y in range(img_h):
            pt = img.getpixel((x, y))
            if pt < 128:
                black += 1
                bottom = y
                if top < 0: top = y
            else:
                white += 1
        if black > white:
            origin_x = x
            maze_height = bottom - top + 1
            break
    # find top side
    for y in range(img_h):
        white, black = 0, 0
        left, right = -1, -1
        for x in range(img_w):
            pt = img.getpixel((x, y))
            if pt < 128:
                black += 1
                right = x
                if left < 0: left = x
            else:
                white += 1
        if black > white:
            origin_y = y
            maze_width = right - left + 1
            break

    return _Location(origin_x, origin_y, maze_width, maze_height)


def _meature(maze_img):
    """
    Meature wall size and room size

    :param maze_img:
    :type maze_img: Image.Image
    :return:
    """
    w, h = maze_img.size

    # measure wall size
    wall_size = 0
    for y in range(h):
        white, black = 0, 0
        for x in range(w):
            pt = maze_img.getpixel((x, y))
            if pt < 128:
                black += 1
            else:
                white += 1
        if white > black:
            wall_size = y
            break

    # measure room size from entry's width
    room_size = 0
    for x in range(w):
        pt = maze_img.getpixel((x, wall_size - 1))
        if pt > 192: room_size += 1

    return wall_size, room_size


def _scan_rooms(maze_img, wall_size, room_size):
    """

    :param maze_img:
    :type maze_img: Image.Image
    :param wall_size:
    :param room_size:
    :return:
    """
    rooms = []

    w, h = maze_img.size
    row_num = math.floor( (h - wall_size) / (room_size + wall_size) )
    col_num = math.floor( (w - wall_size) / (room_size + wall_size) )

    def get_horizontal_weight(y, x0, x1):
        weight = 0
        for x in range(x0, x1):
            weight += maze_img.getpixel((x, y))
        return math.floor(weight / (x1 - x0))

    def get_vertical_weight(x, y0, y1):
        weight = 0
        for y in range(y0, y1):
            weight += maze_img.getpixel((x, y))
        return math.floor(weight / (y1 - y0))

    def get_room_weight(x ,y):
        weight = 0
        for rx in range(room_size):
            for ry in range(room_size):
                pt = maze_img.getpixel((x+rx, y+ry))
                if pt < 192:
                    weight += 1
        return weight * 1.0 / (room_size * room_size)

    entry_pos, exit_pos, key_pos = None, None, None
    for r in range(row_num):
        row = []
        room_y = wall_size + r * (wall_size + room_size)
        for c in range(col_num):
            pos = Position(row=r, col=c)
            room_x = wall_size + c * (wall_size + room_size)
            # get sides weight
            north = get_horizontal_weight(room_y - 1, room_x, room_x + room_size)
            south = get_horizontal_weight(room_y + room_size + 1, room_x, room_x + room_size)
            west = get_vertical_weight(room_x - 1, room_y, room_y + room_size)
            east = get_vertical_weight(room_x + room_size + 1, room_y, room_y + room_size)
            # find enter and exit
            is_entry = (r == 0 and north > 127) or (r == row_num-1 and south > 127) or \
                       (c == 0 and west > 127) or (c == col_num-1 and east > 127)
            is_exit = (r == 0 and 127 > north > 10) or (r == row_num-1 and 127 > south > 10) or \
                      (c == 0 and 127 > west > 10) or (c == col_num-1 and 127 > east > 10)
            if is_entry:
                _logger.info('Entry room => %s', pos)
                # FIXME: no hard-code
                north = 0
                entry_pos = pos
            elif is_exit:
                _logger.info('Exit room => %s', pos)
                # FIXME: no hard-code
                south = 0
                exit_pos = pos
            # check key room
            room_weight = get_room_weight(room_x, room_y)
            if room_weight > 0.1:
                _logger.info('Key room => %s', pos)
                key_pos = pos
            # add room to row
            row.append(Room(
                north=north>127, west=west>127,
                south=south>127, east=east>127,
            ))
        # add row to matrix
        rooms.append(row)

    enter_from = None
    if entry_pos.row == 0:
        enter_from = NORTH
    elif entry_pos.row == row_num - 1:
        enter_from = SOUTH
    elif entry_pos.col == 0:
        enter_from = WEST
    elif entry_pos.col == col_num - 1:
        enter_from = EAST

    return Maze(
        rooms=rooms, row_num=row_num, col_num=col_num,
        enter_from=enter_from, entry_pos=entry_pos, exit_pos=exit_pos, key_pos=key_pos
    ).with_wall_size(wall_size).with_room_size(room_size)

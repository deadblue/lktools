# -*- coding: utf-8 -*-

__author__ = 'deadblue'

NORTH, EAST, SOUTH, WEST = 'n', 'e', 's', 'w'

def get_opposite(direction):
    if direction == NORTH:
        return SOUTH
    elif direction == SOUTH:
        return NORTH
    elif direction == WEST:
        return EAST
    elif direction == EAST:
        return WEST

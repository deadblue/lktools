# -*- coding: utf-8 -*-

__author__ = 'deadblue'

import logging

from lk.maze._types import Maze, Route, Position
from lk.maze._direction import get_opposite

_logger = logging.getLogger(__name__)

def _find_route(maze, route, target_pos, direction_from=None):
    """
    :type maze: Maze
    :type route: Route
    :type target_pos: Position
    :return:
    """
    while target_pos != route.current_position:
        # get room from postion
        room = maze.room_at(route.current_position)
        if direction_from is not None:
            room.set_walked(direction_from)
        _logger.debug('Go to %s => %s', route.current_position, room)

        if room.is_end():
            _logger.debug('Backward for reaching end ...')
            route.backward()
            direction_from = None
        else:
            # select first open direction to go
            directions = room.get_open_directions()
            direction_to = directions[0]
            room.set_walked(direction_to)
            # go to new position
            route.forward(direction_to)
            # update direction_from
            direction_from = get_opposite(direction_to)

def slove(maze):
    """
    :param maze:
    :type maze: Maze
    :return:
    :rtype: Route
    """

    route = Route(start=maze.entry_room)
    # go to key room
    _find_route(maze, route, target_pos=maze.key_room, direction_from=maze.enter_from)
    # reset walk mark
    maze.reset_walked()
    # go to exit room
    _find_route(maze, route, maze.exit_room)
    return route

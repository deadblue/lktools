# -*- coding: utf-8 -*-

__author__ = 'deadblue'

import math

from lk.maze._direction import NORTH, SOUTH, WEST, EAST


class _Location:

    def __init__(self, origin_x, origin_y, width, height):
        self._origin_x = origin_x
        self._origin_y = origin_y
        self._width = width
        self._height = height

    @property
    def left(self): return self._origin_x
    @property
    def right(self): return self._origin_x + self._width
    @property
    def upper(self): return self._origin_y
    @property
    def lower(self): return self._origin_y + self._height

    def __str__(self):
        return '(%d, %d) - (%d, %d)' % (
            self.left, self.upper, self.right, self.lower
        )


class Position:

    def __init__(self, row, col):
        self._row, self._col = row, col

    @property
    def row(self): return self._row
    @property
    def col(self): return self._col

    def next(self, direction):
        """
        Get next postion at specific direction

        :param direction:
        :return:
        :rtype: Position
        """
        if direction == NORTH:
            return Position(row=self._row-1, col=self._col)
        elif direction == SOUTH:
            return Position(row=self._row+1, col=self._col)
        elif direction == WEST:
            return Position(row=self._row, col=self._col-1)
        elif direction == EAST:
            return Position(row=self._row, col=self._col+1)
        else:
            return None

    def __eq__(self, other):
        return isinstance(other, Position) \
               and other.row == self.row \
               and other.col == self.col

    def __str__(self):
        return '(row=%d, column=%d)' % (
            self._row, self._col
        )


class Room:

    def __init__(self, north, east, south, west):
        self._wall = {
            NORTH: north,
            EAST: east,
            SOUTH: south,
            WEST: west
        }
        self._walked = {
            NORTH: False, EAST: False,
            SOUTH: False, WEST: False
        }

    @property
    def north(self): return self._wall[NORTH]
    @property
    def south(self): return self._wall[SOUTH]
    @property
    def east(self): return self._wall[EAST]
    @property
    def west(self): return self._wall[WEST]

    def set_walked(self, direction):
        self._walked[direction] = True

    def reset_walked(self):
        self._walked = {
            NORTH: False, EAST: False,
            SOUTH: False, WEST: False
        }

    def get_open_directions(self):
        directions = []
        for d in (NORTH, EAST, SOUTH, WEST):
            if self._wall[d] and (not self._walked[d]):
                directions.append(d)
        return directions

    def is_end(self):
        return len(self.get_open_directions()) == 0

    def __str__(self):
        return '(N=%d, E=%d, S=%d, W=%d)' % (
            self.north, self.east, self.south, self.west
        )


class Maze:

    def __init__(self, rooms, row_num, col_num, enter_from, entry_pos, exit_pos, key_pos):
        self._rooms = rooms
        self._row = row_num
        self._col = col_num
        self._enter_from = enter_from
        self._entry = entry_pos
        self._exit = exit_pos
        self._key = key_pos
        self._origin_x, self._origin_y = 0, 0
        self._wall_size, self._room_size = 0, 0

    def with_origin(self, x, y):
        self._origin_x = x
        self._origin_y = y
        return self
    def with_wall_size(self, ws):
        self._wall_size = ws
        return self
    def with_room_size(self, rs):
        self._room_size = rs
        return self

    @property
    def origin(self): return self._origin_x, self._origin_y
    @property
    def size(self): return self._row, self._col
    @property
    def enter_from(self): return self._enter_from
    @property
    def entry_room(self): return self._entry
    @property
    def exit_room(self): return self._exit
    @property
    def key_room(self): return self._key

    def reset_walked(self):
        for r in range(self._row):
            for c in range(self._col):
                self._rooms[r][c].reset_walked()

    def room_at(self, pos):
        """
        :param pos:
        :type pos: Position
        :return:
        :rtype: Room
        """
        if pos.row > self._row or pos.col > self._col:
            return None
        return self._rooms[pos.row][pos.col]

    def room_coordinate(self, pos):
        """
        :param pos:
        :type pos: Position
        :return:
        """
        x = self._origin_x + self._wall_size + \
            pos.col * (self._wall_size + self._room_size) + math.floor(self._room_size / 2)
        y = self._origin_y + self._wall_size + \
            pos.row * (self._wall_size + self._room_size) + math.floor(self._room_size / 2)
        return x, y


class Route:

    def __init__(self, start):
        """
        :param start:
        :type start: Position
        """
        self._steps = [start]

    @property
    def current_position(self):
        return self._steps[-1]

    def forward(self, direction):
        self._steps.append(self.current_position.next(direction))

    def backward(self):
        self._steps.pop()

    @property
    def step_num(self): return len(self._steps)

    @property
    def steps(self): return self._steps
# -*- coding: utf-8 -*-

__author__ = 'deadblue'

import logging
logging.basicConfig(
    level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S',
    format='[%(asctime)s][%(levelname)5s] %(name)s - %(message)s'
)

import sys

from PIL import Image, ImageDraw

from lk.maze import scanner, navigator

_logger = logging.getLogger('lk_maze')

def _main(input_file, output_file=None):
    _logger.info('Loading maze image: %s', input_file)
    base_img = Image.open(input_file, 'r')

    maze = scanner.parse( base_img.convert('L') )
    _logger.info('Find maze => %d x %d' % maze.size)

    route = navigator.slove(maze)
    _logger.info('Find route, steps => %d', route.step_num)

    _logger.info('Drawing route on maze ...')
    draw = ImageDraw.Draw(base_img)
    pts = [maze.room_coordinate(step) for step in route.steps]
    draw.line(pts, fill=(255, 0, 0), width=3)

    if output_file is None:
        base_img.show()
        _logger.info('Showing result image and exit!')
    else:
        base_img.save(output_file)
        _logger.info('Result image save to %s', output_file)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage %s <maze_image_file> [output_image_file]' % sys.argv[0])
    else:
        _main(*sys.argv[1:])
